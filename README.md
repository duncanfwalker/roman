Ancient World
=============

Dependencies
------------
* PHP 5.5
* [Composer](http://getcomposer.org)
* [Vagrant](https://www.vagrantup.com/)


Setup
-----
    composer update

Run Webapp
--------------------

    vagrant up
    

Run Acceptance Tests
--------------------

    cd ./tests/ 
    ../vendor/bin/behat
    
    
Run Unit Tests
--------------

    cd ./module/RomanNumerals/test/ 
    ../../../vendor/bin/phpunit
