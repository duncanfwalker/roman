Feature: Convert to Roman numerals
  In order to pass my Latin exam
  As a archaeologist
  I need to convert numbers to Roman numerals

  Scenario Outline: Converting integers to numerals
    Given I am converting numbers to numerals
    When I input number <number>
    Then I see numeral <numeral>
    Examples:
    | number | numeral   |
    | 1      | I         |
    | 5      | V         |
    | 10     | X         |
    | 20     | XX        |
    | 3999   | MMMCMXCIX |

