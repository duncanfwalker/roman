<?php

namespace AncientWorld\Features;

use Behat\Behat\Context\Context;
use Behat\Behat\Context\SnippetAcceptingContext;
use PHPUnit_Framework_Assert as I;
use Zend\Mvc\Application;

/**
 * Defines application features from the specific context.
 */
class FeatureContext implements Context, SnippetAcceptingContext
{
    /** @var Application */
    private static $application;
    private $converter;
    private $number;


    /** @BeforeScenario */
    public static function initializeZendFramework()
    {
        if (self::$application === null) {
            $path = __DIR__ . '/../../../test.config.php';
            self::$application = Application::init(require $path);
        }
    }

    /**
     * @Given /^I am converting numbers to numerals$/
     */
    public function useConverterForRomanNumerals()
    {
        // TODO: Maybe change to a web test - not doing web test in controller tests
        $this->converter = self::$application->getServiceManager()->get('RomanNumerals\Service\Numerals');
    }


    /**
     * @When /^I input number (.*)$/
     */
    public function iEnterNumber($number)
    {
        $this->number = $number;
    }


    /**
     * @Then /^I see numeral (.*)$/
     */
    public function iSeeNumeral($numeral)
    {
            I::assertEquals($numeral, $this->converter->toNumeral($this->number));
    }
}


