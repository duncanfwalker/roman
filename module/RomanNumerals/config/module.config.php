<?php
/**
 * RomanNumerals module
 *
 * @link https://bitbucket.org/duncanfwalker/roman
 * @copyright Duncan Walker <duncfw@gmail.com>
 * @license http://framework.zend.com/license/new-bsd New BSD License
 */
namespace RomanNumerals;

return [
    'service_manager' => [
        'services' => [
            // TODO: Review instantiating here and like this once NumeralService has been refactored
            'RomanNumerals\Service\Numerals' => new Service\NumeralService()
        ]
    ],
    'controllers' => [
        'factories' => [
            'RomanNumerals\Controller\Conversion' => 'RomanNumerals\Controller\ConversionControllerFactory',
        ],
    ],
    'router' => [
        'routes' => [
            'conversion' => [
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => [
                    'route' => '/[:action]',
                    'defaults' => [
                        'controller' => 'RomanNumerals\Controller\Conversion',
                        'action' => 'index',
                    ],
                ],
            ],
        ],
    ],

    'view_manager' => [
        'display_not_found_reason' => false,
        'display_exceptions' => false,
        'doctype' => 'HTML5',
        'not_found_template' => 'error/404',
        'exception_template' => 'error/index',
        'template_map' => [
            'layout/layout' => __DIR__ . '/../view/layout/layout.phtml',
            'error/404' => __DIR__ . '/../view/error/404.phtml',
            'error/index' => __DIR__ . '/../view/error/index.phtml',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
];
