<?php
/**
 * RomanNumerals module
 *
 * @link https://bitbucket.org/duncanfwalker/roman
 * @copyright Duncan Walker <duncfw@gmail.com>
 * @license http://framework.zend.com/license/new-bsd New BSD License
 */
namespace RomanNumerals\Controller;


use RomanNumerals\Form\NumberFilter;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class ConversionControllerFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $controllerLocator)
    {
        $converter = $controllerLocator->getServiceLocator()->get('RomanNumerals\Service\Numerals');
        $filter = new NumberFilter();
        return new ConversionController($converter, $filter );
    }
}