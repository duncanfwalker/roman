<?php
/**
 * RomanNumerals module
 *
 * @link https://bitbucket.org/duncanfwalker/roman
 * @copyright Duncan Walker <duncfw@gmail.com>
 * @license http://framework.zend.com/license/new-bsd New BSD License
 */

namespace RomanNumerals\Controller;

use RomanNumerals\Form\ConversionForm;
use RomanNumerals\Service\NumeralConverterInterface;
use Zend\InputFilter\InputFilter;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class ConversionController extends AbstractActionController
{
    private $converter;
    private $filter;

    /**
     * ConversionController constructor.
     * @param NumeralConverterInterface $converter
     * @param InputFilter $filter
     */
    public function __construct($converter, $filter)
    {
        $this->converter = $converter;
        $this->filter = $filter;
    }


    public function indexAction()
    {
        return new ViewModel(['form'=> new ConversionForm()]);
    }

    public function resultsAction()
    {
        $this->filter->setData($this->params()->fromQuery());

        if (!$this->filter->isValid()) {
            return $this->redirect()->toRoute('conversion');
        }
        return new ViewModel([
            'numeral' => $this->converter->toNumeral($this->filter->getValue('number')),
            'number' => $this->filter->getValue('number')
        ]);

    }
}