<?php
/**
 * RomanNumerals module
 *
 * @link https://bitbucket.org/duncanfwalker/roman
 * @copyright Duncan Walker <duncfw@gmail.com>
 * @license http://framework.zend.com/license/new-bsd New BSD License
 */
namespace RomanNumerals\Form;

use Zend\InputFilter\InputFilter;

class NumberFilter extends InputFilter
{
    public function __construct()
    {
        $this->add(array(
            'name' => 'number',
            'required' => true,
            'validators' => array(
                array(
                    'name' => 'Between',
                    'options' => array(
                        // TODO: get limits from the NumeralService
                        'min' => 1,
                        'max' => 3999,
                    ),
                ),
            ),
        ));
    }
}