<?php
/**
 * RomanNumerals module
 *
 * @link https://bitbucket.org/duncanfwalker/roman
 * @copyright Duncan Walker <duncfw@gmail.com>
 * @license http://framework.zend.com/license/new-bsd New BSD License
 */

namespace RomanNumerals\Form;

use Zend\Form\Form;

class ConversionForm extends Form
{
    /**
     * ConversionForm constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->setAttribute('method', 'GET');

        $this->add([
            'name' => 'number',
            'attributes' => [
                'type' => 'number',
            ],
        ]);

        $this->add(array(
            'name' => 'submit',
            'attributes' => [
                'type' => 'submit',
                'value' => 'Submit',
            ],
        ));
    }
}