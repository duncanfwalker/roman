<?php
/**
 * RomanNumerals module
 *
 * @link https://bitbucket.org/duncanfwalker/roman
 * @copyright Duncan Walker <duncfw@gmail.com>
 * @license http://framework.zend.com/license/new-bsd New BSD License
 */
namespace RomanNumerals\Service;

class NumeralService implements NumeralConverterInterface
{
    const MIN_NUMERAL = 1;
    const MAX_NUMERAL = 3999;

    // TODO: DRY Subtraction Symbols
    private $symbols = [
        1000 => 'M',
        900 =>'CM',
        500 => 'D',
        400 =>'CD',
        100 => 'C',
        90 => 'XC',
        50 => 'L',
        40 => 'XL',
        10 => 'X',
        9 =>'IX',
        5 => 'V',
        4 => 'IV',
        1 => 'I',
    ];

    /**
     * Convert an integer to a Roman numeral.
     * @param int $int number between 1 and 3999
     * @return string Representation of the integer in roman numerals
     */
    public function toNumeral($integer)
    {
        if ($integer > self::MAX_NUMERAL || $integer < self::MIN_NUMERAL) {
            throw new \InvalidArgumentException(
                sprintf(
                    "The integer parameter must be between %d and %d",
                    self::MIN_NUMERAL,
                    self::MAX_NUMERAL
                )
            );
        }

        if (array_key_exists($integer, $this->symbols)) {
            return $this->symbols[$integer];
        } else {
            $string = '';
            foreach ($this->symbols as $value => $label) {
                $quantityOfThisSymbol = floor($integer / $value);
                if ($quantityOfThisSymbol > 0) {
                    $integer = $integer - ( $value * $quantityOfThisSymbol);
                    $string .= str_repeat($label,$quantityOfThisSymbol);
                }
            }
            return $string;
        }
    }


}