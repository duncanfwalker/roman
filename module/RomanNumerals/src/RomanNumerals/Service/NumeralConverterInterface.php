<?php
/**
 * RomanNumerals module
 *
 * @link https://bitbucket.org/duncanfwalker/roman
 * @copyright Duncan Walker <duncfw@gmail.com>
 * @license http://framework.zend.com/license/new-bsd New BSD License
 */

namespace RomanNumerals\Service;


interface NumeralConverterInterface
{
    /**
     * @param int $number
     * @return string
     */
    public function toNumeral($number);
}