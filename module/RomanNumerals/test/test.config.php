<?php
/**
 * RomanNumerals module
 *
 * @link https://bitbucket.org/duncanfwalker/roman
 * @copyright Duncan Walker <duncfw@gmail.com>
 * @license http://framework.zend.com/license/new-bsd New BSD License
 */
return array(
    'modules' => array(
        'RomanNumerals',
    ),

    'module_listener_options' => array(
        'module_paths' => array(
            '../../../module',
            '../../../vendor',
        ),

        'config_glob_paths' => array(
            'config/autoload/{{,*.}global,{,*.}local}.php',
        ),
    )
);
