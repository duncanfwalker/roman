<?php
namespace RomanNumeralsTest\Form;


use RomanNumerals\Form\NumberFilter;

class NumberFilterTest extends \PHPUnit_Framework_TestCase
{
    private $filter;

    protected function setUp()
    {
        parent::setUp();
        $this->filter = new NumberFilter();
    }

    public function testGetDataDoesNotAllowTooBigNumbers()
    {
        $this->filter->setData(['number'=>4000]);
        $this->assertFalse($this->filter->isValid());
    }

    public function testGetDataDoesNotAllowTooSmallNumbers()
    {
        $this->filter->setData(['number'=>0]);
        $this->assertFalse($this->filter->isValid());
    }
    public function testGetDataAllowsGoodNumbers()
    {
        $this->filter->setData(['number'=>10]);
        $this->assertTrue($this->filter->isValid());
    }

    public function testTakesNumberRangeFromNumeralService()
    {
        // TODO: write test
        $this->markTestIncomplete();
    }
}
