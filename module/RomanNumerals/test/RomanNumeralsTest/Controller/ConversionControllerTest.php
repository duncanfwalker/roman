<?php
/**
 * Created by PhpStorm.
 * User: duncanwalker
 * Date: 04/12/2015
 * Time: 22:35
 */

namespace RomanNumeralsTest\Controller;


use RomanNumerals\Service\NumeralService;
use Zend\Http\Request;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;

class ConversionControllerTest extends AbstractHttpControllerTestCase
{
    public function setUp()
    {
        $this->setApplicationConfig(\Bootstrap::getApplicationConfig());
        parent::setUp();
    }
    public function testFormActionCanBeAccessed()
    {
        $this->dispatch('/');
        $this->assertResponseStatusCode(200);
        $this->assertControllerClass('ConversionController');
    }

    public function testResultsActionCanBeAccessed()
    {
        $this->dispatch('/results',Request::METHOD_GET,['number'=>10]);
        $this->assertResponseStatusCode(200);
        $this->assertControllerClass('ConversionController');
    }

    public function testResultActionCallNumeralsService()
    {
        /** @var NumeralService|\PHPUnit_Framework_MockObject_MockObject $service */
        $service = $this->getMock(NumeralService::class, array('toNumeral'));
        $service->expects($this->once())->method('toNumeral')->with('10');


        $serviceManager = $this->getApplicationServiceLocator();
        $serviceManager->setAllowOverride(true);
        $serviceManager->setService('RomanNumerals\Service\Numerals', $service);

        $this->dispatch('/results',Request::METHOD_GET,['number'=>10]);
    }

    public function testResultActionValidatesForm()
    {
        /** @var NumeralService|\PHPUnit_Framework_MockObject_MockObject $service */
        $service = $this->getMock(NumeralService::class, array('toNumeral'));
        $service->expects($this->any())->method('toNumeral')->with('10');


        $serviceManager = $this->getApplicationServiceLocator();
        $serviceManager->setAllowOverride(true);
        $serviceManager->setService('RomanNumerals\Service\Numerals', $service);

        $this->dispatch('/results',Request::METHOD_GET,['number'=>10000]);
        $this->assertRedirect('/');
    }

}
