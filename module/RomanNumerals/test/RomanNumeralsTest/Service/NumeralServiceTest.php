<?php
/**
 * Created by PhpStorm.
 * User: duncanwalker
 * Date: 04/12/2015
 * Time: 21:45
 */

namespace RomanNumeralsTest\Service;


use RomanNumerals\Service\NumeralService;

class NumeralServiceTest extends \PHPUnit_Framework_TestCase
{
    /** @var  NumeralService */
    private $converter;

    public function setup() {
        $this->converter = new NumeralService();
    }

    public function testToNumeralWithSingleSymbol()
    {
        $this->assertEquals('X',$this->converter->toNumeral(10));
    }

    public function testToNumeralWithAddingSymbol()
    {
        $this->assertEquals('XI',$this->converter->toNumeral(11));
    }

    public function testToNumeralWithSubtractingSymbol()
    {
        $this->assertEquals('IV',$this->converter->toNumeral(4));
    }

    public function testToNumeralWithNonAdjacentSubtractingSymbol()
    {
        $this->assertEquals('IX',$this->converter->toNumeral(9));
    }

    public function testToNumeralWithAddingAndSubtractingSymbols()
    {
        $this->assertEquals('XIX',$this->converter->toNumeral(19));
    }

    public function testToNumeralWithManyAddingSymbols()
    {
        $this->assertEquals('CCCXXXIII',$this->converter->toNumeral(333));
    }


    public function testToNumeralThrowsExceptionWhenNumberIsTooBig()
    {
        $this->setExpectedException('InvalidArgumentException');
        $this->assertEquals($this->converter->toNumeral(0),0);
    }

    public function testToNumeralThrowsExceptionWhenNumberIsTooSmall()
    {
        $this->setExpectedException('InvalidArgumentException');
        $this->assertEquals($this->converter->toNumeral(40000),0);
    }


}
